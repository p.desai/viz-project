var div = d3.select("div.rank").append("div")
.attr("class", "tooltip")
.style("opacity", 0);

// set the dimensions and margins of the graph
var marginloli = { top: 10, right: 100, bottom: 30, left: 100 },
widthloli = 700 - marginloli.left - marginloli.right,
heightloli = 600 - marginloli.top - marginloli.bottom;

// append the svgloli object to the body of the page
var svgloli = d3.select("#lolipop")
.append("svg")
.attr("width", widthloli + marginloli.left + marginloli.right)
.attr("height", heightloli + marginloli.top + marginloli.bottom)
.append("g")
.attr("transform",
    "translate(" + marginloli.left + "," + marginloli.top + ")");

// Parse the Data
d3.csv("../data/euro power rank.csv", function (data) {


// Add X axis
var x = d3.scaleLinear()
    .domain([0, 25])
    .range([0, widthloli]);
svgloli.append("g")
    .attr("transform", "translate(0," + heightloli + ")")
    .call(d3.axisBottom(x))

// Y axis
var y = d3.scaleBand()
    .range([0, heightloli])
    .domain(data.map(function (d) { return d.Country; }))
    .padding(1);
svgloli.append("g")
    .call(d3.axisLeft(y))

// Lines
svgloli.selectAll("myline")
    .data(data)
    .enter()
    .append("line")
    .attr("x1", function (d) { return x(d.Before); })
    .attr("x2", function (d) { return x(d.Mid); })
    .attr("y1", function (d) { return y(d.Country); })
    .attr("y2", function (d) { return y(d.Country); })
    .attr("stroke", "grey")
    .attr("stroke-width", "1px")

// Circles of variable 1
svgloli.selectAll("mycircle")
    .data(data)
    .enter()
    .append("circle")
    .attr("cx", function (d) { return x(d.Before); })
    .attr("cy", function (d) { return y(d.Country); })
    .attr("r", "6")
    .style("fill", "#69b3a2")
    .on("mouseover", function (d) {
        d3.select(this).transition()
            .duration('50')
            .attr('opacity', '.85');
        div.transition()
            .duration(200)
            .style("opacity", .9);
        div.html(d.Before)
            .style("left", (d3.event.pageX) + "px")
            .style("top", (d3.event.pageY - 28) + "px");
    })
    .on("mouseout", function (d) {
        div.transition()
            .duration(500)
            .style("opacity", 0);
    });


// Circles of variable 2
svgloli.selectAll("mycircle")
    .data(data)
    .enter()
    .append("circle")
    .attr("cx", function (d) { return x(d.Mid); })
    .attr("cy", function (d) { return y(d.Country); })
    .attr("r", "6")
    .style("fill", "#4C4082")
    .on("mouseover", function (d) {
        d3.select(this).transition()
            .duration('50')
            .attr('opacity', '.85');
        div.transition()
            .duration(200)
            .style("opacity", .9);
        div.html(d.Mid)
            .style("left", (d3.event.pageX) + "px")
            .style("top", (d3.event.pageY - 28) + "px");
    })
    .on("mouseout", function (d) {

        div.transition()
            .duration(500)
            .style("opacity", 0);
    });

svgloli.append('rect')
    .attr('x', 367)
    .attr('y', 5)
    .attr('width', 150)
    .attr('height', 50)
    .attr('stroke', 'black')
    .attr('fill', 'none');
svgloli.append("circle").attr("cx", 380).attr("cy", 20).attr("r", 6).style("fill", "#69b3a2")
svgloli.append("circle").attr("cx", 380).attr("cy", 40).attr("r", 6).style("fill", "#404080")
svgloli.append("text").attr("x", 400).attr("y", 20).text("Before Euro 2020").style("font-size", "15px").attr("alignment-baseline", "middle")
svgloli.append("text").attr("x", 400).attr("y", 40).text("At Mid Euro 2020").style("font-size", "15px").attr("alignment-baseline", "middle")
});

