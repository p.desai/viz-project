// d3.require("d3-array@2", "d3-color@2", "d3-scale-chromatic@2");

// var d = [];
// set the dimensions and margins of the graph
var marginline = { top: 10, right: 30, bottom: 30, left: 60 },
  widthline = 460 - marginline.left - marginline.right,
  heightline = 400 - marginline.top - marginline.bottom;

// append the svgline object to the body of the page
var svgline = d3.select("#line")
  .append("svg")
  .attr("width", widthline + marginline.left + marginline.right)
  .attr("height", heightline + marginline.top + marginline.bottom)
  .append("g")
  .attr("transform",
    "translate(" + marginline.left + "," + marginline.top + ")");

//Read the data
d3.json("../data/shotsdata.json", function (err, data) {

  // List of groups (here I have one group per column)
  var allGroup = d3.keys(data);
  // console.log(allGroup);

  // add the options to the button
  d3.select("#selectButton")
    .selectAll('myOptions')
    .data(allGroup)
    .enter()
    .append('option')
    .text(function (d) { return d; }) // text showed in the menu
    .attr("value", function (d) { return d; }) // corresponding value returned by the button

  // A color scale: one color for each group
  var myColor =
    d3.scaleOrdinal()
      .domain(allGroup)
      .range(["lightblue", "pink", "purple", "yellow"]);

  // Add X axis --> it is a date format
  //
  var xline = d3.scalePoint()
    .domain(data.Italy.map(function (i, j) { return i.match; }))
    .range([0, widthline]);
    
  svgline.append("g")
    .attr("transform", "translate(0," + heightline + ")")
    .call(d3.axisBottom(xline))
    .append("text")
    .text("hello");
    svgline.append("text")      // text label for the x-axis
    .attr("x",  widthline / 2 )
    .attr("y",  heightline + 30)
    .style("text-anchor", "middle")
    .text("Match Number");

  // Add Y axis
  var yline = d3.scaleLinear()
    .domain([0, 30])
    .range([heightline, 0]);
  svgline.append("g")
    .call(d3.axisLeft(yline));
    svgline.append("text")      // text label for the y-axis
    .attr("y",-30 )
    .attr("x",50 - (heightline / 2))
    .attr("transform", "rotate(-90)")
    .style("text-anchor", "end")
    .style("font-size", "16px")
    .text("Total Shots");

  // Initialize line with first group of the list
  //data.filter(function(d){return d.name==allGroup[0]})
  var line = svgline
    .append('g')
    .append("path")
    .datum(data[d3.keys(data).filter(function (d) { return d == allGroup[0] })])
    .attr("d", d3.line()
      .x(function (d) { return xline(d.match); })
      .y(function (d) { return yline(d.shots); })
    )
    .attr("stroke", function (d) { return myColor("Italy") })
    .style("stroke-width", 3)
    .style("fill", "none");

  var markers = svgline.selectAll("markers")
    .data(data[d3.keys(data).filter(function (d) { return d == allGroup[0] })])
    .enter()
    .append("circle")
    .attr("fill", function (d) {
      if (d.shots > d.shotsO) {
        return "red";
      }
      else {
        return "green";
      }
    })
    .attr("stroke", "none")
    .attr("cx", function (d) { return xline(d.match); })
    .attr("cy", function (d) { return yline(d.shots); })
    .attr("r", function(d){return d.size});
  // .append("polygon")
  //   .attr("points", "0,0 100,0 50,50")

  svgline.append("circle").attr("cx", 290).attr("cy", 20).attr("r", 6).style("fill", "red")
  svgline.append("circle").attr("cx", 290).attr("cy", 40).attr("r", 6).style("fill", "green")
  svgline.append("text").attr("x", 300).attr("y", 20).text("Aggresive").style("font-size", "11px").attr("alignment-baseline", "middle")
  svgline.append("text").attr("x", 300).attr("y", 40).text("Defensive").style("font-size", "11px").attr("alignment-baseline", "middle")

  // A function that update the chart
  function update(selectedGroup) {

    // Create new data with the selection?
    var dataFilter = data[d3.keys(data).filter(function (d) { return d == selectedGroup })]

    // Give these new data to update line
    line
      .datum(dataFilter)
      .transition()
      .duration(1000)
      .attr("d", d3.line()
        .x(function (d) { return xline(d.match); })
        .y(function (d) { return yline(d.shots); })
      )
      .attr("stroke", function (d) { return myColor(selectedGroup) });

    markers.data(dataFilter)
      .transition()
      .duration(1000)
      .attr("fill", function (d) {
        if (d.shots > d.shotsO) {
          return "red";
        }
        else {
          return "green";
        }
      })
      .attr("cx", function (d) { return xline(d.match); })
      .attr("cy", function (d) { return yline(d.shots); })
      .attr("r", function(d){return d.size})
      ;
  }

  // When the button is changed, run the updateChart function
  d3.select("#selectButton").on("change", function (d) {
    // recover the option that has been chosen
    var selectedOption = d3.select(this).property("value")
    // run the updateChart function with this selected option
    update(selectedOption)
  })

});

