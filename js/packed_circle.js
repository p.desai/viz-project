
var svgpack = d3.select("svg#jojo2"),
    marginpack = 20,
    diameterpack = +svgpack.attr("width"),
    g = svgpack.append("g").attr("transform", "translate(" + diameterpack / 2 + "," + diameterpack / 2 + ")");

var color = d3.scaleLinear([])
    .domain([-1, 5])
    .range(["hsl(100,70%,69%)", "hsl(222,12%,12%)"])
    .interpolate(d3.interpolateHcl);

var pack = d3.pack()
    .size([diameterpack - marginpack, diameterpack - marginpack])
    .padding(2);



d3.json("../data/league.json", function(error, root) {
  if (error) throw error;

  root = d3.hierarchy(root)
      .sum(function(d) { return d.size; })
      .sort(function(a, b) { return b.value - a.value; });

  var focus = root,
      nodes = pack(root).descendants(),
      view;

  var circle = g.selectAll("circle")
    .data(nodes)
    .enter().append("circle")
      .attr("class", function(d) { return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root"; })
      .style("fill", function(d) { return d.children ? color(d.depth) : null; })
      .on("click", function(d) { if (focus !== d) zoom(d), d3.event.stopPropagation(); });


  var text = g.selectAll("text")
    .data(nodes)
    .enter().append("text")
      .attr("class", "label")
      .style("fill-opacity", function(d) { return d.parent === root ? 1 : 0; })
      .style("display", function(d) { return d.parent === root ? "inline" : "none"; })
      .text(function(d) { return d.data.name; });


  var node = g.selectAll("circle,text");


  svgpack
      .on("click", function() { zoom(root); });

      console.log("circle is "+ root)

  zoomTo([root.x, root.y, root.r * 2 + marginpack]);

  function zoom(d) {
    var focus0 = focus; focus = d;
    var transition = d3.transition()
        .duration(d3.event.altKey ? 7500 : 750)
        .tween("zoom", function(d) {
          var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + marginpack]);
          return function(t) { zoomTo(i(t)); };
        });

    transition.selectAll("text")
      .filter(function(d) { if (typeof d !== "undefined" )return d.parent === focus || this.style.display === "inline"; })
        .style("fill-opacity", function(d) { return d.parent === focus ? 1 : 0; })
        .on("start", function(d) { if (d.parent === focus) this.style.display = "inline"; })
        .on("end", function(d) { if (d.parent !== focus) this.style.display = "none"; });

  }

  function zoomTo(v) {
    var k = diameterpack / v[2]; view = v;
    node.attr("transform", function(d) { return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")"; });
    circle.attr("r", function(d) { return d.r * k; });
  }
});
