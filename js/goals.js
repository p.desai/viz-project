var radius = 350,
	numRounds = 4,
	segmentWidth = radius / (numRounds + 1);

var partition = d3.layout.partition()
	.sort(null)
	.size([2 * Math.PI, radius]) // x maps to angle, y to radius
.value(function(d) {
	return 1;
}); //Important!

var arc = d3.svg.arc()
	.startAngle(function(d) {
		return d.x;
	})
	.endAngle(function(d) {
		return d.x + d.dx;
	})
	.innerRadius(function(d) {
		return d.y;
	})
	.outerRadius(function(d) {
		return d.y + d.dy;
	});

function translateSVG(x, y) {
	return 'translate(' + x + ',' + y + ')';
}


//prints name 
function rotateSVG(a, x, y) {
	a = a * 180 / Math.PI;
	return 'rotate(' + a + ')';
	return 'rotate(' + a + ',' + x + ',' + y + ')';
}

function arcSVG(mx0, my0, rx, ry, xrot, larc, sweep, mx1, my1) {
	return 'M' + mx0 + ',' + my0 + ' A' + rx + ',' + ry + ' ' + xrot + ' ' + larc + ',' + sweep + ' ' + mx1 + ',' + my1;
}

var label = function(d) {
	if (d.x === 0 && d.y === 0)
		return '';
	var t = rotateSVG(d.x + 0.5 * d.dx - Math.PI * 0.5, 0, 0);
	t += translateSVG(d.y + 0.5 * d.dy, 0);
	t += d.x >= Math.PI ? rotateSVG(Math.PI) : '';
	return t;
}

	function team(d) {
		return d.name.split(' ')[0];
	}

	function fullname(d) {
		return d.name;
	}

	function result(d) {
		var m = d.match;
		var res = '';
		if (m !== undefined) {
			for (var i = 1; i <= 5; i++) {
				if (m['w' + i] !== null && m['l' + i] !== null && m['w' + i] !== undefined && m['l' + i] !== undefined)
					res += m['w' + i] + ' - ' + m['l' + i] + ' ';
			}
		}
		return res;
	}

	function playerHover(d) {
		var c = team(d);
		d3.selectAll('g#player-labels text')
			.style('fill', 'white');

		// Highlight this player + children
		d3.select('g#player-labels text.' + c + '.round-' + d.round)
			.style('fill', 'yellow');

		if (d.round != 1) {
			c = team(d.children[0]);
			d3.select('g#player-labels text.' + c + '.round-' + +(d.round - 1))
				.style('fill', 'yellow');

			c = team(d.children[1]);
			d3.select('g#player-labels text.' + c + '.round-' + +(d.round - 1))
				.style('fill', 'yellow');
		}

		// var l = team(d.children[1]);
		// d3.selectAll('g#player-labels text.'+l)
		//   .style('fill', 'gray');


		var m = d.match;
		if (m !== undefined) {
			d3.select('#result').text( fullname(d.children[0]) + ' beat ' + fullname(d.children[1]));
			d3.select('#score').text(result(d));
		}
	}

var xCenter = radius,
	yCenter = radius;
var svggoal = d3.select('svg').append('g').attr('transform', translateSVG(xCenter, yCenter));

d3.json('../data/goalsdata.json', function(err, root) {
	// console.log(root);
	var chart = svggoal.append('g');
	chart.datum(root).selectAll('g')
		.data(partition.nodes)
		.enter()
		.append('g');

	// We use three groups: segments, round labels & player labels. This is to achieve a layering effect.

	// Segments
	chart.selectAll('g')
		.append('path')
		.attr('id', function(d, i) {
			return i === 0 ? team(d) : d.goals;
		})
		.attr('d', arc)
		.on('mouseover', playerHover);

	// Round labels
	var rounds = ['Round Of 16', 'Quater Final', 'Semi Final', 'Final'];
	var roundLabels = svggoal.append('g').attr('id', 'round-labels');
	roundLabels.selectAll('path')
		.data(rounds)
		.enter()
		.append('path')
		.attr('d', function(d, i) {
			var offset = (numRounds - i + 0.5) * segmentWidth - 10;
			return arcSVG(-offset, 0, offset, offset, 0, 1, 1, offset, 0);
		})
		.style({
			'fill': 'none',
			'stroke': 'none'
		})
		.attr('id', function(d, i) {
			return 'round-label-' + +(i + 1);
		});

	roundLabels.selectAll('text')
		.data(rounds)
		.enter()
		.append('text')
		.append('textPath')
		.attr('xlink:href', function(d, i) {
			return '#round-label-' + +(i + 1);
		})
		.attr('startOffset', '50%')
		.text(function(d) {
			return d;
		});


	// Player labels
	var playerLabels = svggoal.append('g').attr('id', 'player-labels');
	playerLabels.datum(root).selectAll('g')
		.data(partition.nodes)
		.enter()
		.append('text')
		.text(function(d, i) {
			return i === 0 ? team(d) : d.name.slice(0, 3);
		})
		
        .attr("y", "25")
		.attr('transform', label)
		.attr('dy', '0.4em')
		.attr('class', function(d) {
			return team(d) + ' round-' + +(d.round);
		});

	var imgs = svggoal.append('g').attr('id', 'player-labels');
	     imgs.datum(root).selectAll('g')
		.data(partition.nodes)
		.enter()	
		.append("svg:image")
		.attr("xlink:href", function(d){ return d.link;})
		.attr("x", "-15")
        .attr("y", "-15")
        .attr("width", "30")
        .attr("height", "30")
	    .attr('transform', label)
		.attr('dy', '0.0em')
});