
var RadarChart = {
    draw: function(id, d, options){
    var cfg = {
       radius: 5,
       w: 600,
       h: 600,
       factor: 1,
       factorLegend: .85,
       levels: 3,
       maxValue: 0,
       radians: 2 * Math.PI,
       opacityArea: 0.0,
       ToRight: 5,
       TranslateX: 80,
       TranslateY: 30,
       ExtraWidthX: 100,
       ExtraWidthY: 100,
       color: d3.scaleOrdinal(d3.schemeCategory10)
      };
      
      if('undefined' !== typeof options){
        for(var i in options){
          if('undefined' !== typeof options[i]){
            cfg[i] = options[i];
          }
        }
      }
      cfg.maxValue = Math.max(cfg.maxValue, d3.max(d, function(i){return d3.max(i.map(function(o){return o.value;}))}));
      var allAxis = d[0].map(function(i, j){return i['axis'];});
      var total = allAxis.length;
      var radius = cfg.factor*Math.min(cfg.w/2, cfg.h/2);
      var Format = d3.format('.0f');
      d3.select(id).select("svg").remove();
      
      var g = d3.select(id)
              .append("svg")
              .attr("width", cfg.w+cfg.ExtraWidthX)
              .attr("height", cfg.h+cfg.ExtraWidthY)
              .append("g")
              .attr("transform", "translate(" + cfg.TranslateX + "," + cfg.TranslateY + ")");
  
      var tooltip;
      
      //Circular segments
      for(var j=0; j<cfg.levels; j++){
        var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
        g.selectAll(".levels")
         .data(allAxis)
         .enter()
         .append("svg:line")
         .attr("x1", function(d, i){return levelFactor*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
         .attr("y1", function(d, i){return levelFactor*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
         .attr("x2", function(d, i){return levelFactor*(1-cfg.factor*Math.sin((i+1)*cfg.radians/total));})
         .attr("y2", function(d, i){return levelFactor*(1-cfg.factor*Math.cos((i+1)*cfg.radians/total));})
         .attr("class", "line")
         .style("stroke", "grey")
         .style("stroke-opacity", "0.75")
         .style("stroke-width", "1px")
         .attr("transform", "translate(" + (cfg.w/2-levelFactor) + ", " + (cfg.h/2-levelFactor) + ")");
      }
  
      //Text indicating at what value each level is
      // for Attacks
      for(var j=0; j<cfg.levels; j++){
        var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
        g.selectAll(".levels")
         .data([1]) //dummy data
         .enter()
         .append("svg:text")
         .attr("x", function(d){return levelFactor*(1-cfg.factor*Math.sin(0));})
         .attr("y", function(d){return levelFactor*(1-cfg.factor*Math.cos(0));})
         .attr("class", "legend")
         .style("font-family", "sans-serif")
         .style("font-size", "10px")
         .attr("transform", "translate(" + (cfg.w/2-levelFactor + cfg.ToRight) + ", " + (cfg.h/2-levelFactor) + ")")
         .attr("fill", "#737373")
         .text(Format((j+1)*cfg.range[0]/cfg.levels));
      }
      // for Goals
      for(var j=0; j<cfg.levels; j++){
        var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
        g.selectAll(".levels")
         .data([1]) //dummy data
         .enter()
         .append("svg:text")
         .attr("x", function(d){return -levelFactor*(1-cfg.factor*Math.sin(0));})
         .attr("y", function(d){return levelFactor*(1-cfg.factor*Math.cos(0));})
         .attr("class", "legend")
         .style("font-family", "sans-serif")
         .style("font-size", "10px")
         .attr("transform", "translate(" + (cfg.w/2+cfg.ToRight) + ", " + (cfg.h/2-levelFactor/3-cfg.ToRight) + ")")
         .attr("fill", "#737373")
         .text(Format((j+1)*cfg.range[1]/cfg.levels));
      }
      // for Balls Recovered
      for(var j=0; j<cfg.levels; j++){
        var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
        g.selectAll(".levels")
         .data([1]) //dummy data
         .enter()
         .append("svg:text")
         .attr("x", function(d){return -levelFactor*(1-cfg.factor*Math.sin(0));})
         .attr("y", function(d){return levelFactor*(1-cfg.factor*Math.cos(0));})
         .attr("class", "legend")
         .style("font-family", "sans-serif")
         .style("font-size", "10px")
         .attr("transform", "translate(" + (cfg.w/3+levelFactor*.38+14*cfg.ToRight) + ", " + (cfg.h/2+levelFactor*.8-cfg.ToRight) + ")")
         .attr("fill", "#737373")
         .text(Format((j+1)*cfg.range[2]/cfg.levels));
      }
      // for Saves
      for(var j=0; j<cfg.levels; j++){
        var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
        g.selectAll(".levels")
         .data([1]) //dummy data
         .enter()
         .append("svg:text")
         .attr("x", function(d){return levelFactor*(1-cfg.factor*Math.sin(0));})
         .attr("y", function(d){return levelFactor*(1-cfg.factor*Math.cos(0));})
         .attr("class", "legend")
         .style("font-family", "sans-serif")
         .style("font-size", "10px")
         .attr("transform", "translate(" + (cfg.w/2-levelFactor*.44-2*cfg.ToRight) + ", " + (cfg.h/2+levelFactor*.8+2*cfg.ToRight) + ")")
         .attr("fill", "#737373")
         .text(Format((j+1)*cfg.range[3]/cfg.levels));
      }
      // for Fouls Committed
      for(var j=0; j<cfg.levels; j++){
        var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
        g.selectAll(".levels")
         .data([1]) //dummy data
         .enter()
         .append("svg:text")
         .attr("x", function(d){return levelFactor*(1+cfg.factor*Math.sin(0));})
         .attr("y", function(d){return levelFactor*(1-cfg.factor*Math.cos(0));})
         .attr("class", "legend")
         .style("font-family", "sans-serif")
         .style("font-size", "10px")
         .attr("transform", "translate(" + (cfg.w/2-3*cfg.ToRight) + ", " + (cfg.h/2-levelFactor/3+4*cfg.ToRight) + ")")
         .attr("fill", "#737373")
         .text(Format((j+1)*cfg.range[4]/cfg.levels));
      }
      
      series = 0;
  
      var axis = g.selectAll(".axis")
              .data(allAxis)
              .enter()
              .append("g")
              .attr("class", "axis");
  
      axis.append("line")
          .attr("x1", cfg.w/2)
          .attr("y1", cfg.h/2)
          .attr("x2", function(d, i){return cfg.w/2*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
          .attr("y2", function(d, i){return cfg.h/2*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
          .attr("class", "line")
          .style("stroke", "grey")
          .style("stroke-width", "1px");
  
      axis.append("text")
          .attr("class", "legend")
          .text(function(d){return d})
          .style("font-family", "sans-serif")
          .style("font-size", "11px")
          .attr("text-anchor", "middle")
          .attr("dy", "1.5em")
          .attr("transform", function(d, i){return "translate(0, -10)"})
          .attr("x", function(d, i){return cfg.w/2*(1-cfg.factorLegend*Math.sin(i*cfg.radians/total))-60*Math.sin(i*cfg.radians/total);})
          .attr("y", function(d, i){return cfg.h/2*(1-Math.cos(i*cfg.radians/total))-20*Math.cos(i*cfg.radians/total);});
  
      
          // to create the polygon
      d.forEach(function(y, x){
        dataValues = [];    // polygon points
        g.selectAll(".nodes")
          .data(y, function(j, i){
            dataValues.push([
              cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.range[i])*cfg.factor*Math.sin(i*cfg.radians/total)), 
              cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.range[i])*cfg.factor*Math.cos(i*cfg.radians/total))
            ]);
            // console.log({1:range[i],2:j.value,3:parseFloat(Math.max(j.value, 0))/cfg.maxValue,4:parseFloat(Math.max(j.value, 0))/cfg.range[i]});
          });
        dataValues.push(dataValues[0]);
        g.selectAll(".area")
                       .data([dataValues])
                       .enter()
                       .append("polygon")
                       .attr("class", "radar-chart-serie"+series)
                       .style("stroke-width", "3px")
                       .style("stroke", cfg.color(series))
                       .attr("points",function(d) {
                           var str="";
                           for(var pti=0;pti<d.length;pti++){
                               str=str+d[pti][0]+","+d[pti][1]+" ";
                           }
                           return str;
                        })
                       .style("fill", function(j, i){return cfg.color(series)})
                       .style("fill-opacity", cfg.opacityArea)
                       .on('mouseover', function (d){
                                          z = "polygon."+d3.select(this).attr("class");
                                          g.selectAll("polygon")
                                           .transition(200)
                                           .style("fill-opacity", 0.05); 
                                          g.selectAll(z)
                                           .transition(200)
                                           .style("fill-opacity", .5);
                                        })
                       .on('mouseout', function(){
                                          g.selectAll("polygon")
                                           .transition(200)
                                           .style("fill-opacity", cfg.opacityArea);
                       });
        series++;
      });
      series=0;
  
  
      d.forEach(function(y, x){
        g.selectAll(".nodes")
          .data(y).enter()
          .append("svg:circle")
          .attr("class", "radar-chart-serie"+series)
          .attr('r', cfg.radius)
          .attr("alt", function(j){return Math.max(j.value, 0)})
          .attr("cx", function(j, i){
            dataValues.push([
              cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.range[i])*cfg.factor*Math.sin(i*cfg.radians/total)), 
              cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.range[i])*cfg.factor*Math.cos(i*cfg.radians/total))
            ]);
            return cfg.w/2*(1-(Math.max(j.value, 0)/cfg.range[i])*cfg.factor*Math.sin(i*cfg.radians/total));
          })
          .attr("cy", function(j, i){
            return cfg.h/2*(1-(Math.max(j.value, 0)/cfg.range[i])*cfg.factor*Math.cos(i*cfg.radians/total));
          })
          .attr("data-id", function(j){return j.axis})
          .style("fill", cfg.color(series)).style("fill-opacity", .9)
          .on('mouseover', function (d){
                      newX =  parseFloat(d3.select(this).attr('cx')) - 10;
                      newY =  parseFloat(d3.select(this).attr('cy')) - 5;
                      
                      // tooltip
                      //     .attr('x', newX)
                      //     .attr('y', newY)
                      //     .text(Format(d.value))
                      //     .transition(200)
                      //     .style('opacity', 1);
                          
                      z = "polygon."+d3.select(this).attr("class");
                      g.selectAll("polygon")
                          .transition(200)
                          .style("fill-opacity", 0.1); 
                      g.selectAll(z)
                          .transition(200)
                          .style("fill-opacity", .7);
                    })
          .on('mouseout', function(){
                      tooltip
                          .transition(200)
                          .style('opacity', 0);
                      g.selectAll("polygon")
                          .transition(200)
                          .style("fill-opacity", cfg.opacityArea);
                    })
          .append("svg:title")
          .text(function(j){return Math.max(j.value, 0)});
  
        series++;
      });
      //Tooltip
      tooltip = g.append('text')
                 .style('opacity', 0)
                 .style('font-family', 'sans-serif')
                 .style('font-size', '13px');
    }
  };


  var w = 500,
			h = 500;

		var colorscale = d3.scaleOrdinal(d3.schemeCategory10)

		//Legend titles
		var LegendOptions = ['Italy', 'England', 'Denmark', 'Spain'];



		//Data
		// var data = [], p = [[], [], [], []];
		// d3.csv("Euro 2020 - Radar.csv", function(csv){
		//     data=csv;

		//     data.forEach(d => {
		//       d['Attacks'] = parseInt(d['Attacks']);
		//       d['Goals'] = parseInt(d['Goals']);
		//       d['Balls Recovered'] = parseInt(d['Balls Recovered']);
		//       d['Saves'] = parseInt(d['Saves']);
		//       d['Fouls Committed'] = parseInt(d['Fouls Committed']);
		//     });

		//     for(var i in data) {
		// 		var k = 0;
		//         for(var j in data[i]) {
		//             if( j != 'Country') {
		//                 var obj = {};
		//                 obj['axis'] = j;
		//                 obj['value'] = data[i][j];
		//                 p[i][k++] = obj;
		//             }
		//         }
		//     }
		// });

		var p = [
			[
				{ 'axis': 'Attacks', 'value': 383 },
				{ 'axis': 'Goals', 'value': 13 },
				{ 'axis': 'Balls Recovered', 'value': 291 },
				{ 'axis': 'Saves', 'value': 9 },
				{ 'axis': 'Fouls Committed', 'value': 93 },
			],
			[
				{ 'axis': 'Attacks', 'value': 296 },
				{ 'axis': 'Goals', 'value': 11 },
				{ 'axis': 'Balls Recovered', 'value': 264 },
				{ 'axis': 'Saves', 'value': 16 },
				{ 'axis': 'Fouls Committed', 'value': 71 },
			],
			[
				{ 'axis': 'Attacks', 'value': 280 },
				{ 'axis': 'Goals', 'value': 12 },
				{ 'axis': 'Balls Recovered', 'value': 265 },
				{ 'axis': 'Saves', 'value': 18 },
				{ 'axis': 'Fouls Committed', 'value': 75 },
			],
			[
				{ 'axis': 'Attacks', 'value': 432 },
				{ 'axis': 'Goals', 'value': 13 },
				{ 'axis': 'Balls Recovered', 'value': 267 },
				{ 'axis': 'Saves', 'value': 9 },
				{ 'axis': 'Fouls Committed', 'value': 72 },
			]
		],
			range = [500, 15, 375, 20, 100];

		// console.log(p);

		//Options for the Radar chart, other than default
		var mycfg = {
			w: w,
			h: h,
			maxValue: 0.6,
			levels: 6,
			ExtraWidthX: 300,
			range: range,
			radius: 4
		}

		//Call function to draw the Radar chart
		//Will expect that data is in %'s
		RadarChart.draw("#chart2", p, mycfg);

		////////////////////////////////////////////
		/////////// Initiate legend ////////////////
		////////////////////////////////////////////

		var svgradar = d3.select('#radarmain')
			.selectAll('svg')
			.append('svg')
			.attr("width", w + 300)
			.attr("height", h)

		//Create the title for the legend
		var text = svgradar.append("text")
			.attr("class", "title")
			.attr('transform', 'translate(90,0)')
			.attr("x", w - 70)
			.attr("y", 10)
			.attr("font-size", "12px")
			.attr("fill", "#404040")
			.text("Countries");

		//Initiate Legend	
		var legend = svgradar.append("g")
			.attr("class", "legend")
			.attr("height", 100)
			.attr("width", 200)
			.attr('transform', 'translate(90,20)')
			;
		//Create colour squares
		legend.selectAll('rect')
			.data(LegendOptions)
			.enter()
			.append("rect")
			.attr("x", w - 65)
			.attr("y", function (d, i) { return i * 20; })
			.attr("width", 10)
			.attr("height", 10)
			.style("fill", function (d, i) { return colorscale(i); })
			;
		//Create text next to squares
		legend.selectAll('text')
			.data(LegendOptions)
			.enter()
			.append("text")
			.attr("x", w - 52)
			.attr("y", function (d, i) { return i * 20 + 9; })
			.attr("font-size", "11px")
			.attr("fill", "#737373")
			.text(function (d) { return d; })
			;
