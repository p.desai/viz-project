
    var showing = "Avg. Possession";
    var x = null, dataP = null;
    var txxx = d3.select("div#txxx");
    txxx.html("Current View " + "<h4>"+ showing +"</h4>")	

    function whichData(val) {
      showing = val;
      console.log(val);
      txxx.transition()		
      .duration(200)		
      .style("opacity", .9);
      
      txxx.html("Current View " + "<h4>"+ showing +"</h4>")	

      // graphShowing();
      updateChart();
    }



    // set the dimensions and margins of the graph
    var margin = { top: 5, right: 30, bottom: 90, left: 100 },
      width = 600 - margin.left - margin.right,
      height = 700 - margin.top - margin.bottom;

    // append the svgBar object to the body of the page
    var svgBar = d3.select("#bar")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

    // svgBar.selectAll("*").remove();

    function updateXScale(data) {
      x = d3.scaleLinear()
        .domain([0, d3.max(data, function (d) { return d[showing]; })])
        .range([0, width - 10]);
    }

    function updateChart() {
      updateXScale(dataP);

      // Animation
      svgBar.selectAll("rect")
        .transition()
        .duration(1000)
        .attr("id",function(d){
          return d.Country.slice(0, 3);
        })
        .attr("width", function (d) { return x(d[showing]); })
        .delay(function (d, i) { return (i * 10) });

      svgBar
        .selectAll("text.value")
        .transition()
        .duration(1000)
        .attr("x", function (d) { return d[showing] == 0 ? x(d[showing]) + 15 : x(d[showing]) - 5; })
        // .tween("text", function (d) {
        //   var i = d3.interpolate(parseFloat(this.textContent), d[showing]);
        //   // console.log({'a':parseFloat(this.textContent),'b':d[showing]});
        //   // console.log(i(0.5));
        //   return function (t) {
        //     this.textContent = Math.round(i(t) * 100) / 100;
        //     // console.log(t);
        //   };
        //   // this.textContent = Math.round(i(1)*100)/100;
        // })
        .text(function (d) { return d[showing]; })
        .delay(function (d, i) { return (i * 10) });
    }

    // Parse the Data
    d3.csv("../data/Euro 2020 - Bar chart.csv", function (data) {
      dataP = data;

      dataP.forEach(d => {
        d['Avg. Possession'] = parseFloat(d['Avg. Possession']);
        d['Avg. Passes'] = parseFloat(d['Avg. Passes']);
        d['Avg. Pass Accuracy'] = parseFloat(d['Avg. Pass Accuracy']);
        d['Avg. Fouls'] = parseFloat(d['Avg. Fouls']);
        d['Avg. Yellow Cards'] = parseFloat(d['Avg. Yellow Cards']);
        d['Avg. Red Cards'] = parseFloat(d['Avg. Red Cards']);
      });

      // X axis
      updateXScale(data);

      // console.log(data[:][showing]);
      // data.sort((a, b) => d3.descending(a[showing], b[showing]));

      // x = d3.scaleLinear()
      //         .domain([0, max])
      //         .range([0, width]);
      svgBar.append("g")
        .attr("transform", "translate(0,50)")
        .call(d3.axisTop(x))
        .attr("opacity", 0)
        .selectAll("text")
        // .attr("transform", "translate(5,0)")
        .style("text-anchor", "center");

      // Add Y axis
      var y = d3.scaleBand()
        .range([0, height])
        .domain(data.map(function (d) { return d.Country; }))
        .padding(0.2);
      svgBar.append("g")
        .attr("transform", "translate(0,50)")
        .call(d3.axisLeft(y))
        .selectAll("text")
        .attr("transform", "translate(-10,0)");

      // Bars
      svgBar.selectAll("mybar")
        .data(data)
        .enter()
        .append("rect")
        .attr("height", y.bandwidth())
        .attr("y", function (d) { return y(d.Country); })
        .attr("fill", "#abcdef")
        // no bar at the beginning thus:
        .attr("x", function (d) { return x(0); })
        .attr("width", function (d) { return x(0); }) // always equal to 0
        .attr("transform", "translate(0,50)");
      // Bars Annotation
      svgBar.selectAll("mybar")
        .data(data)
        .enter()
        .append("text")
        .attr("class", "value")
        .text(function (d) { return d[showing]; })
        // .attr("x", function(d){ return d[showing]==0? x(d[showing])+15 : x(d[showing])-5;})
        .attr("y", function (d) { return y(d.Country) + y.bandwidth() * .75;; })
        .attr("font-family", "sans-serif")
        .attr("font-size", "14px")
        .attr("fill", "white")
        .attr("text-anchor", "end")
        .attr("transform", "translate(0,50)");

    

      updateChart();

    })
