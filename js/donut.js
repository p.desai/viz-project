
    // set the dimensions and margins of the graph
    var widthdonut = 600,
      heightdonut = 600,
      margindonut = 40;

    // The radiusdonut of the pieplot is half thewidthdonut or half the heightdonut (smallest one). I subtract a bit of margindonut.
    var radiusdonut = Math.min(widthdonut, heightdonut) / 2 - margindonut;
    var radiusdonutIn = radiusdonut * 2 / 3;
    var radiusdonutInner = radiusdonut / 3;

    // append the svgdonut object to the div called 'donut'
    var svgdonut = d3.select("#donut")
      .append("svg")
      .attr("width",widthdonut)
      .attr("height", heightdonut)
      .append("g")
      .attr("transform", "translate(" +widthdonut / 2 + "," + heightdonut / 2 + ")");

    // Create dummy data

    d3.json("../data/goalsdata.json", function (data) {

      var secondLevel = [];
      var firstLevel = [];
      var total = 0;
      for (let i in data) {
        let temp = 0;
        for (let j in data[i]) {
          temp += data[i][j].goals;
          secondLevel.push({ key: data[i][j].time, goals: data[i][j].goals, id: data[i][j].id });
        }
        firstLevel.push({ key: i, goals: temp, id: data[i][0].id });
        total += temp;
      }

      // console.log(firstLevel);
      // console.log(secondLevel);
      // console.log(total);

      // set the color scale
      var color = d3.scaleOrdinal()
        .domain(d3.keys(data))
        .range([
          "#f0f921",
          
          "#fada24",
          
         " #febd2a",
          
         " #fba238",
          
          "#f48849",
          
"  #e97158"         , 
         " #db5c68",
          
          "#cc4778",
          
         " #b83289",
          
         " #a31e9a",
          
         " #8b0aa5",
          
         " #6f00a8",
          
          "#5302a3",
          
        "  #350498",
          
         " #0d0887"
          ]);
      
      console.log("this is " + d3.scaleOrdinal)

      // Compute the position of each group on the pie:
      var pie = d3.pie()
        .value(function (d) { return d.value.goals; })
        .sort(null);
      var dataFirst = pie(d3.entries(firstLevel));
      var dataSecond = pie(d3.entries(secondLevel));

      // console.log(dataFirst);

      var arc1 = d3.arc()
        .innerRadius(radiusdonutIn)         // This is the size of the donut hole
        .outerRadius(radiusdonut),
        arc2 = d3.arc()
          .innerRadius(radiusdonutInner)         // This is the size of the donut hole
          .outerRadius(radiusdonutIn);

      // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.

      var p = svgdonut.selectAll('whatever')
        .select("circle")
        .data(dataFirst)
        .enter()
        .append('text')
        .text("Goals : \n"+total)
        .attr("class", "goal");

      svgdonut
        .selectAll('whatever')
        .data(dataSecond)
        .enter()
        .append('path')
        .attr('d', arc1)
        .attr('fill', function (d) { return (color(d.data.key)) })
        .attr("stroke", "white")
        .style("stroke-width", "4px")
        .on("mouseover", function (d) {
          d3.select(this).transition().duration("50").attr("opacity", "0.7");
          p.text("Goals : \n"+d.data.value.goals);
        }
        )
        .on("mouseout", function (d) {
          d3.select(this).transition().duration("100").attr("opacity", "1");
          p.text("Goals : \n"+total);
        }
        );

      // console.log(arc1.centroid(dataSecond[0]));

      svgdonut.selectAll('whatever')
        .data(dataSecond)
        .enter()
        .append('text')
        .text(function (d) { return d.data.value.key; })
        .attr("transform", function (d) { 
          let xy = arc1.centroid(d);
          // (xy[0] + 0.5 * d.dx - Math.PI * 0.5) * 180 / Math.PI
          return "translate(" + xy + ")rotate(0)";
        })
        .style("text-anchor", "middle")
        .attr("color", "black")
        .style("font-weight", "bold")
        .on("mouseover", function (d) {
          d3.select(this).transition().duration("50").attr("opacity", "0.7");
          p.text("Goals : \n"+d.data.value.goals);
        }
        )
        .on("mouseout", function (d) {
          d3.select(this).transition().duration("100").attr("opacity", "1");
          p.text("Goals : \n"+total);
        }
        );

      svgdonut
        .selectAll('whatever')
        .data(dataFirst)
        .enter()
        .append('path')
        .attr('d', arc2)
        .attr('fill', function (d) { return (color(d.data.key)) })
        .attr("stroke", "white")
        .style("stroke-width", "4px")
        .on("mouseover", function (d) {
          d3.select(this).transition().duration("50").attr("opacity", "0.7");
          p.text("Goals : \n"+d.data.value.goals);
        }
        )
        .on("mouseout", function (d) {
          d3.select(this).transition().duration("100").attr("opacity", "1");
          p.text("Goals : \n"+total);
        }
        );;

      svgdonut.selectAll('whatever')
        .data(dataFirst)
        .enter()
        .append('text')
        .text(function (d) { return d.data.value.key; })
        .attr("transform", function (d) { return "translate(" + arc2.centroid(d) + ")rotate(0)"; })
        .style("text-anchor", "middle")
        .attr("color", "black")
        .style("font-weight", "bold");

    });

