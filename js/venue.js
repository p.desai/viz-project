
var tt = d3v3.select("div#tooltipx")
.style("opacity", 0);
var marginatt = {top: 50, right: 20, bottom: 60, left: 90},
    widthatt = 600 - marginatt.left - marginatt.right,
    heightatt = 400 - marginatt.top - marginatt.bottom;

var x = d3v3.scale.linear()
    .range([0, widthatt]);

var y = d3v3.scale.linear()
    .range([heightatt, 0]);

var xAxis = d3v3.svg.axis()
    .scale(x)
    .tickSize(1)
    .orient("bottom");
   

var xMinorAxis = d3v3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3v3.svg.axis()
    .scale(y)
    .tickSize(2)
    .orient("left");

var line = d3v3.svg.line()
    .x(function(d) { return x(d.no); })
    .y(function(d) { return y(d["Total Attendence"]); });



var svgv = d3v3.select("#venue").append("svg")
    .attr("width", widthatt + marginatt.left + marginatt.right)
    .attr("height", heightatt + marginatt.top + marginatt.bottom)
  .append("g")
    .attr("transform", "translate(" + marginatt.left + "," + marginatt.top + ")");


function make_y_axis() {
  return d3v3.svg.axis()
      .scale(y)
      .orient("left")
      //.ticks(5)
}

//reading in CSV which contains data
d3v3.csv("../data/venue.csv", function(error, data) {
  data.forEach(function(d) {
    //console.log(d.date_time)
    //d.date = parseDate.parse(d.date_time);
    d["Total Attendence"] = parseInt(d["Total Attendence"]);
	//console.log(d["Total Attendence"]);
    //d.total_km = +d.total_km;
    //console.log(d.total_km);
  });

  //using imported data to define extent of x and y domains
  x.domain([1,51]);
  y.domain([0,d3v3.max(data, function(d) { return d["Total Attendence"]; })]);


  console.log();
// Draw the y Grid lines
	svgv.append("g")            
		.attr("class", "grid")
        .style("stroke","grey")        
        .style("opacity",0.5)
		.call(make_y_axis()
			.tickSize(-widthatt, 0, 0)
			.tickFormat("")
		)
  
  svgv.append("path")
      .datum(data)
      .attr("class", "venueline")
      .attr("d", line)
      .style("fill","none")
      .style("stroke","steelblue")
      .style("stroke-width","1");

//taken from http://bl.ocks.org/mbostock/3887118
//and http://www.d3v3noob.org/2013/01/change-line-chart-into-scatter-plot.html
//creating a group(g) and will append a circle and 2 lines inside each group
var g = svgv.selectAll()
        .data(data).enter().append("g");

   //The markers on the line
	 g.append("circle")
         //circle radius is increased
        .attr("r", 4.5)
        .attr("cx", function(d) { return x(d.no); })
        .attr("cy", function(d) { return y(d["Total Attendence"]); });
   
   //The horizontal dashed line that appears when a circle marker is moused over
	 g.append("line")
        .attr("class", "x")
        .attr("id", "dashedLine")
        .style("stroke", "steelblue")
        .style("stroke-dasharray", "3,3")
        .style("opacity", 0)
        .attr("x1", function(d) { return x(d.no); })
        .attr("y1", function(d) { return y(d["Total Attendence"]); })
		    //d3v3.min gets the min date from the date x-axis scale
		    .attr("x2", function(d) { return x(1); })
        .attr("y2", function(d) { return y(d["Total Attendence"]); });

  //The vertical dashed line that appears when a circle marker is moused over
	g.append("line")
        .attr("class", "y")
        .attr("id", "dashedLine")
        .style("stroke", "steelblue")
        .style("stroke-dasharray", "3,3")
        .style("opacity", 0)
        .attr("x1", function(d) { return x(d.no); })
        .attr("y1", function(d) { return y(d["Total Attendence"]); })
		    .attr("x2", function(d) { return x(d.no); })
        .attr("y2", heightatt);
    
   //circles are selected again to add the mouseover functions
 	 g.selectAll("circle")
			.on("mouseover", function(d) {		
            tt.transition()		
               .duration(200)		
               .style("opacity", .9);	
            tt.html("<h4>"+d.Match+"</h4>" +  "<h4>Attendee = " + d["Total Attendence"] + "</h4>" + "<h4>Venue = " + d.Place+"</h4>")	
               .style("left", (d3v3.event.pageX - 20) + "px")
      		     .style("top", (d3v3.event.pageY + 6) + "px");
	          //selects the horizontal dashed line in the group
			      d3v3.select(this.nextElementSibling).transition()		
                .duration(200)		
                .style("opacity", .9);
            //selects the vertical dashed line in the group
			      d3v3.select(this.nextElementSibling.nextElementSibling).transition()		
                .duration(200)		
                .style("opacity", .9);	
            })	
				
      .on("mouseout", function(d) {		
            tt.transition()		
               .duration(500)		
               .style("opacity", 0);

			      d3v3.select(this.nextElementSibling).transition()		
                .duration(500)		
                .style("opacity", 0);

			      d3v3.select(this.nextElementSibling.nextElementSibling).transition()		
                .duration(500)		
                .style("opacity", 0);	
        });

svgv.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + heightatt + ")")
      .call(xAxis)
	    .selectAll(".tick text")
      //.call(wrap, 35);

svgv.append("g")
    .attr("class","xMinorAxis")
    .attr("transform", "translate(0," + heightatt + ")")
    .style({ 'stroke': 'Black', 'fill': 'none', 'stroke-width': '1px'})
    .call(xMinorAxis)
    .selectAll("text").remove();

//http://www.d3v3noob.org/2012/12/adding-axis-labels-to-d3v3js-graph.html
svgv.append("text")      // text label for the x-axis
        .attr("x", widthatt / 2 )
        .attr("y",  heightatt + 40)
        .style("text-anchor", "middle")
        .text("Match Number");

svgv.append("text")      // text label for the y-axis
        .attr("y",30 - marginatt.left)
        .attr("x",50 - (heightatt / 2))
        .attr("transform", "rotate(-90)")
        .style("text-anchor", "end")
        .style("font-size", "16px")
        .text("Total Attendee");

//http://www.d3v3noob.org/2013/01/adding-title-to-your-d3v3js-graph.html



svgv.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    
function wrap(text, widthatt) {
  text.each(function() {
    var text = d3v3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineheightatt = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > widthatt) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineheightatt + dy + "em").text(word);
      }
    }
  });
}

});
