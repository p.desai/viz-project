var diameter = 600,
    format = d3v3.format(",d");

// var color = d3v3.scale.ordinal()
//     .domain(["Sqoop", "Pig", "Apache", "a", "b", "c", "d", "e", "f", "g"])
//     .range(["steelblue", "pink", "lightgreen", "violet", "orangered", "green", "orange", "skyblue", "gray", "aqua"]);

var color = d3v3.scale.category20()
var bubble = d3v3.layout.pack()
    .sort(null)
    .size([diameter, diameter])
    .padding(10);

var svgbubble = d3v3.select("#chart3").append("svg")
    .attr("width", diameter)
    .attr("height", diameter)
    .attr("class", "bubble");

// Datas
var Goals = {
    "children": [
        {
            "name": "Ronaldo",
            "factor": 1.0,
            "size": 5
        },
        {
            "name": "Schick",
            "factor": 0.9,
            "size": 5
        },
        {
            "name": "Lukaku",
            "factor": 0.8,
            "size": 4
        },
        {
            "name": "Forsberg",
            "factor": 0.7,
            "size": 4
        },
        {
            "name": "Benzema",
            "factor": 0.6,
            "size": 4
        },
        {
            "name": "Kane",
            "factor": 0.5,
            "size": 4
        },
        {
            "name": "Dolberg",
            "factor": 0.4,
            "size": 3
        },
        {
            "name": "Shaqiri",
            "factor": 0.3,
            "size": 3
        },
        {
            "name": "Sterling",
            "factor": 0.2,
            "size": 3
        },
        {
            "name": "Morata",
            "factor": 0.1,
            "size": 3
        }
    ]
};

var Pass = {
    "children": [
        {
            "name": "James",
            "size": 99
        },
        {
            "name": "Boyata",
            "size": 97
        },
        {
            "name": "Hummels",
            "size": 97
        },
        {
            "name": "Eric Garcia",
            "size": 96
        },
        {
            "name": "Witsel",
            "size": 96
        },
        {
            "name": "Bastoni",
            "size": 96
        },
        {
            "name": "Laporte",
            "size": 95
        },
        {
            "name": "Ruedigre",
            "size": 95
        },
        {
            "name": "Ćaleta-Car",
            "size": 95
        },
        {
            "name": "Vermaelen",
            "size": 94
        }
    ]
};

var Assists = {
    "children": [
        {
            "name": "Zuber",
            "size": 4
        },
        {
            "name": "Olmo",
            "size": 3
        },
        {
            "name": "Shaw",
            "size": 3
        },
        {
            "name": "Verrati",
            "size": 3
        },
        {
            "name": "Hojbjerg",
            "size": 3
        },
        {
            "name": "Depay",
            "size": 2
        },
        {
            "name": "Alaba",
            "size": 2
        },
        {
            "name": "De Bruyne",
            "size": 2
        },
        {
            "name": "Sarabia",
            "size": 2
        },
        {
            "name": "Berardi",
            "size": 2
        }
    ]
};

var BallsRecovered = {
    "children": [
        {
            "name": "Akanji",
            "size": 46
        },
        {
            "name": "Jorginho",
            "size": 46
        },
        {
            "name": "Matviyenko",
            "size": 44
        },
        {
            "name": "Di Lorenzo",
            "size": 40
        },
        {
            "name": "Walker",
            "size": 40
        },
        {
            "name": "Kjær",
            "size": 39
        },
        {
            "name": "De Vrij",
            "size": 38
        },
        {
            "name": "Chiellini",
            "size": 37
        },
        {
            "name": "Hojbjerg",
            "size": 36
        },
        {
            "name": "Coufal",
            "size": 35
        }
    ]
};

var Saves = {
    "children": [
        {
            "name": "Sommer",
            "size": 21
        },
        {
            "name": "Uğurcan Çakır",
            "size": 18
        },
        {
            "name": "Schmeichel",
            "size": 18
        },
        {
            "name": "Ward",
            "size": 18
        },
        {
            "name": "Pickford",
            "size": 16
        },
        {
            "name": "Bushchan",
            "size": 15
        },
        {
            "name": "Dimitrievski",
            "size": 14
        },
        {
            "name": "Vaclik",
            "size": 14
        },
        {
            "name": "Hradecky",
            "size": 14
        },
        {
            "name": "Courtois",
            "size": 14
        }
    ]
};

var Fouls = {
    "children": [
        {
            "name": "Delaney",
            "size": 15
        },
        {
            "name": "Di Lorenzo",
            "size": 13
        },
        {
            "name": "Phillips",
            "size": 12
        },
        {
            "name": "Jorginho",
            "size": 11
        },
        {
            "name": "T. Hazard",
            "size": 11
        },
        {
            "name": "Laimer",
            "size": 11
        },
        {
            "name": "Kucka",
            "size": 10
        },
        {
            "name": "Shaw",
            "size": 10
        },
        {
            "name": "Olmo",
            "size": 9
        },
        {
            "name": "Laporte",
            "size": 9
        }
    ]
};

var txxxx =  d3.select("div#txxxx");
txxxx.html("Current View " + "<h4>"+ "Total Goals" +"</h4>")	
// creating Nodes
var node = svgbubble.selectAll(".node")
    .data(bubble.nodes(classes(Goals))
        .filter(function (d) {
            return !d.children;
        }))
    .enter().append("g")
    .attr("class", "node")
    .attr("transform", function (d) {
        return "translate(" + d.x + "," + d.y + ")";
    });

// tooltips
node.append("title")
    .text(function (d) {
        return d.classname + ": " + format(d.value);
    });
// showing bubbles
node.append("circle")
    .attr("r", function (d) {
        return d.r;
    })
    .style("fill", function (d, i) {
        return color(i);
    })
   // .style("stroke", d => d3.max(d.value) ? "red" : "transparent")
    .on('mouseover', function (d) {
        d3.select(this)
            .append("svg:image")
            .attr("xlink:href", function (d) { return "../logo/" + d.classname + ".jpg"; })
            .attr("width", function (d) { return 2 * d.r; })
            .attr("height", function (d) { return 2 * d.r; })
    });

// adding text to bubbles
// node.append("text")
//     .attr("dy", "0.2em")
//     .text(function (d) {
//         return d.classname + "\n" + d.value
//     })
//     .style("text-anchor", "middle")
//     .attr("font-family", "sans-serif")
//     .attr("fill", "white")
//     .attr("font-size", function (d) {
//         return d.r / 5;
//     });

node.attr('id', 'player-names')
   .append("svg:image")
   .attr("xlink:href", function(d){ return "../logo/"+d.classname+".png";})
   .attr("x",  function(d){ return -d.r; })
   .attr("y",  function(d){ return -d.r; })
   .attr("width", function(d){ return 2*d.r; })
   .attr("height", function(d){ return 2*d.r; })
   //.attr('transform', label)
   .attr('dy', '0.0em');


// Returns a flattened hierarchy containing all leaf nodes under the root.
function classes(root) {
    var classes = [];

    function recurse(name, node) {
        if (node.children) node.children.forEach(function (child) {
            recurse(node.name, child);
        });
        else classes.push({
            packagename: name,
            classname: node.name,
            value: node.size,
        });
    }

    recurse(null, root);
    return {
        children: classes
    };
}

//d3v3.select(self.frameElement).style("height", diameter + "px");

//My Refer;
// var click = 0;

// function changevalues() {
//     click++;
//     if (click == 1) changebubble(root2);
//     else if (click == 2) changebubble(root3);
//     else changebubble(root4);

// }

//update function
function changebubble(root) {
    var node = svgbubble.selectAll(".node")
        .data(
            bubble.nodes(classes(root)).filter(function (d) { return !d.children; }),
            function (d) { return d.classname } // key data based on classname to keep object constancy
        );

    // capture the enter selection
    var nodeEnter = node.enter()
        .append("g")
        .attr("class", "node")
        .attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });

    // re-use enter selection for circles
    nodeEnter
        .append("circle")
        .attr("r", function (d) { return d.r; })
        .style("fill", function (d, i) { return color(i); })

    // re-use enter selection for titles
    nodeEnter
        .append("title")
        .text(function (d) {
            return d.classname + ": " + format(d.value);
        });

    node.select("circle")
        .transition().duration(1000)
        .attr("r", function (d) {
            return d.r;
        })
        .style("fill", function (d, i) {
            return color(i);
        });

    // node.selectAll("text").remove();

    // node.append("text")
    //     .attr("dy", "0.2em")
    //     .text(function (d) {
    //         return d.classname + "\n" + d.value
    //     })
    //     .style("text-anchor", "middle")
    //     .attr("font-family", "sans-serif")
    //     .attr("fill", "white")
    //     .attr("font-size", function (d) {
    //         return d.r / 5;
    //     });

    // images.selectAll("svg:image").remove();
    // images.remove();
    node.attr('id', 'player-names')
        .append("svg:image")
        .attr("xlink:href", function(d){ return "../logo/"+d.classname+".png";})
        .attr("x",  function(d){ return -d.r; })
        .attr("y",  function(d){ return -d.r; })
        .attr("width", function(d){ return 2*d.r; })
        .attr("height", function(d){ return 2*d.r; })
       // .attr('transform', label)
        .attr('dy', '0.0em');


    node.transition().attr("class", "node")
        .attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });

    node.exit().remove();

    // Returns a flattened hierarchy containing all leaf nodes under the root.
    function classes(root) {
        var classes = [];
        function recurse(name, node) {
            if (node.children) node.children.forEach(function (child) {
                recurse(node.name, child);
            });
            else classes.push({
                packagename: name,
                classname: node.name,
                value: node.size
            });
        }

        recurse(null, root);
        return {
            children: classes
        };
    }

    //d3v3.select(self.frameElement).style("height", diameter + "px");
}

function updateBubble1() {txxxx.html("Current View " + "<h4>"+ "Total Goals" +"</h4>"); changebubble(Goals); };
function updateBubble2() {txxxx.html("Current View " + "<h4>"+ "Pass Accuracy" +"</h4>"); changebubble(Pass); };
function updateBubble3() {txxxx.html("Current View " + "<h4>"+ "Total Assist" +"</h4>"); changebubble(Assists); };
function updateBubble4() { txxxx.html("Current View " + "<h4>"+ "Total Balls Recovered" +"</h4>");changebubble(BallsRecovered); };
function updateBubble5() { txxxx.html("Current View " + "<h4>"+ "Total Saves" +"</h4>");changebubble(Saves); };
function updateBubble6() { txxxx.html("Current View " + "<h4>"+ "Total Fouls" +"</h4>");changebubble(Fouls); };

d3v3.select("#dataset1").on("click", updateBubble1);
d3v3.select("#dataset2").on("click", updateBubble2);
d3v3.select("#dataset3").on("click", updateBubble3);
d3v3.select("#dataset4").on("click", updateBubble4);
d3v3.select("#dataset5").on("click", updateBubble5);
d3v3.select("#dataset6").on("click", updateBubble6);